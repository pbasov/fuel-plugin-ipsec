define ipsec_policy (
  $ensure = present,
  $source,
  $destination,
  $src_port = undef,
  $dst_port = undef,
  $key,
  $type,
  $namespace = undef,
) {
  file { "/etc/ipsec-tools.d/${name}":
    ensure => file,
    content = template(ipsec-tools.d.erb)
  }
  

}