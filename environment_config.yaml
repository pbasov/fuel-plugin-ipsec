attributes:
  # Show contrail only in supported network config
  metadata:
    restrictions:
      - condition: "not (cluster:net_provider == 'neutron' and networking_parameters:segmentation_type != 'vlan')"
        message: "Please use Neutron with tunneling segmentation."
  contrail_asnum:
    value: '64512'
    label: 'AS Number'
    description: 'AS number for BGP communication'
    weight: 20
    type: "text"
    regex:
      source: '^(?:(6553[0-5])|(655[0-2]\d)|(65[0-4]\d{2})|(6[0-4]\d{3})|([1-5]\d{4})|([1-9]\d{1,3})|([1-9]))$'
      error: "Invalid AS number"
  contrail_gateways:
    value: '10.109.3.250'
    label: 'GW IP'
    description: 'Comma separated IP addresses of BGP peers.'
    weight: 40
    type: "text"
    regex:
      source: '^(?:\d|1?\d\d|2[0-4]\d|25[0-5])(?:\.(?:\d|1?\d\d|2[0-4]\d|25[0-5])){3}(?:,\s*(?:\d|1?\d\d|2[0-4]\d|25[0-5])(?:\.(?:\d|1?\d\d|2[0-4]\d|25[0-5])){3})*$'
      error: "Invalid IP address list"
  contrail_external:
    value: '10.100.1.0/24'
    label: 'External IP CIDR'
    description: 'The CIDR for Contrail external network'
    weight: 50
    type: "text"
    regex:
      source: '^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\/([1-9]|[1-2]\d|3[0-2])$'
      error: "Invalid CIDR"
  contrail_route_target:
    value: '10000'
    label: 'Route Target'
    description: 'The route target number for Contrail external network'
    weight: 60
    type: "text"
    regex:
      source: '^(?:(6553[0-5])|(655[0-2]\d)|(65[0-4]\d{2})|(6[0-4]\d{3})|([1-5]\d{4})|([1-9]\d{1,3})|([1-9]))$'
      error: "Invalid target"
  contrail_global_dpdk:
    value: false
    label: "Enable DPDK feature for this environment."
    description: "Enable this option to unlock the DPDK feature.\nNOTE: You still have to assign DPDK-role to compute nodes to enable DPDK on them."
    weight: 70
    type: "checkbox"
  hugepages_size:
    type: 'select'
    weight: 120
    value: '2'
    label: 'Hugepage size'
    description: 'Choose the size of hugepages that will be used for dpdk feature. Check if 1GB pages are supported on target compute node. # grep pdpe1gb /proc/cpuinfo | uniq'
    values:
      - data: '2'
        label: '2M'
      - data: '1024'
        label: "1G"
    restrictions:
      - action: hide
        condition: not (settings:contrail.metadata.enabled == true and settings:contrail.contrail_global_dpdk.value == true)
  hugepages_amount:
    value: '10'
    label: 'Hugepages amount (%)'
    description: 'The amount of memory allocated on each compute-node for hugepages in percent'
    weight: 130
    type: 'text'
    regex:
      source: '^[1-9][0-9]?$|^100$'
      error: "Invalid amount of percent"
    restrictions:
      - condition: "not (settings:contrail.metadata.enabled == true and settings:contrail.contrail_global_dpdk.value == true)"
        action: "hide"
  vrouter_core_mask:
    value: "0x3"
    label: "CPU pinning"
    description: "Hexadecimal mask that determines how many and what exactly processors will be used for dpdk vrouter"
    weight: 131
    type: "text"
    regex:
      source: '0[xX][0-9a-fA-F]+'
      error: "Invalid hexadecimal value"
    restrictions:
      - condition: "not (settings:contrail.metadata.enabled == true and settings:contrail.contrail_global_dpdk.value == true)"
        action: "hide"
  patch_nova:
    value: true
    label: "Patch Nova"
    description: "Patch Nova packages for dpdk-vrouter"
    weight: 140
    type: "checkbox"
    restrictions:
      - action: hide
        condition: not (settings:contrail.metadata.enabled == true  and settings:contrail.contrail_global_dpdk.value == true)
  install_contrail_qemu_lv:
    value: true
    label: "Install Qemu and Libvirt from Contrail"
    description: "Install Qemu and Libvirt packages from contrail repository"
    weight: 150
    type: "checkbox"
    restrictions:
      - condition: "not (settings:contrail.metadata.enabled == true and settings:contrail.contrail_global_dpdk.value == true)"
        action: "hide"